function mvvRoute(origin, destination) {
    var d = new Date();
    var year = d.getFullYear();
    var month = ('0' + (d.getMonth() + 1)).slice(-2);
    var day = ('0' + d.getDate()).slice(-2);
    var hour = ('0' + d.getHours()).slice(-2);
    var minute = ('0' + d.getMinutes()).slice(-2);
    var sec = ('0' + d.getSeconds()).slice(-2);
    date = year + "-" + month + "-" + day
    time = hour + ":" + minute + ":" + sec

    // https://www.bahn.de/buchung/fahrplan/suche#sts=false&so=Rohrbach(Ilm)&zo=M%C3%BCnchen%20Hbf&soid=A%3D1%40O%3DRohrbach(Ilm)&zoid=A%3D1%40O%3DM%C3%BCnchen%20Hbf&hd=2024-07-09T07:55:16
    var url = "https://www.bahn.de/buchung/fahrplan/suche#sts=false" +
        "&so=" + origin + "&zo=" + destination +
        "&soid=A%3D1%40O%3D" + origin + "&zoid=A%3D1%40O%3D" + destination +
        "&hd=" + date + "T" + time

    var win = window.open(url, '_blank');
    win.focus();
}

function setDestination(dest) {
    imageSize = document.getElementsByClassName('small').length > 0 ? 'small' : 'big';

    // Update mode buttons
    if (dest === 'home') {
        // Update button states
        document.getElementById('home-button').className = "mode-button selected";
        document.getElementById('away-button').className = "mode-button unselected";
        document.getElementById('home').className = imageSize + " selected";
        document.getElementById('away').className = imageSize + " unselected";

        // Update Au route
        document.getElementById('au-munich').href = "javascript:mvvRoute('M&uuml;nchen%20Hbf', 'Hopfenhalle,%20Au%20i.d.%20Hallertau');";
        document.getElementById('au-munich').alt = "Munich -> Au";
        document.getElementById('au-munich').title = "Munich -> Au";
        document.getElementById('au-munich-direction').textContent = "Munich → Au";
        document.querySelector('#au-munich .origin-img').src = "/images/munich.png";
        document.querySelector('#au-munich .destination-img').src = "/images/au.png";

        // Update Munich route
        document.getElementById('rohrbach-munich').href = "javascript:mvvRoute('M&uuml;nchen%20Hbf', 'Rohrbach(Ilm)');";
        document.getElementById('rohrbach-munich').alt = "Munich -> Rohrbach";
        document.getElementById('rohrbach-munich').title = "Munich -> Rohrbach";
        document.getElementById('rohrbach-munich-direction').textContent = "Munich → Rohrbach";
        document.querySelector('#rohrbach-munich .origin-img').src = "/images/munich.png";
        document.querySelector('#rohrbach-munich .destination-img').src = "/images/rohrbach(ilm).png";

        // Update Freising route
        document.getElementById('au-freising').href = "javascript:mvvRoute('Freising', 'Hopfenhalle,%20Au%20i.d.%20Hallertau');";
        document.getElementById('au-freising').alt = "Freising -> Au";
        document.getElementById('au-freising').title = "Freising -> Au";
        document.getElementById('au-freising-direction').textContent = "Freising -> Au";
        document.querySelector('#au-freising .origin-img').src = "/images/freising.png";
        document.querySelector('#au-freising .destination-img').src = "/images/au.png";

        // Update Freising route
        document.getElementById('freising-munich').href = "javascript:mvvRoute('M&uuml;nchen%20Hbf', 'Freising');";
        document.getElementById('freising-munich').alt = "Munich -> Freising";
        document.getElementById('freising-munich').title = "Munich -> Freising";
        document.getElementById('freising-munich-direction').textContent = "Munich → Freising";
        document.querySelector('#freising-munich .origin-img').src = "/images/munich.png";
        document.querySelector('#freising-munich .destination-img').src = "/images/freising.png";
    } else {
        // Update button states
        document.getElementById('home-button').className = "mode-button unselected";
        document.getElementById('away-button').className = "mode-button selected";
        document.getElementById('home').className = imageSize + " unselected";
        document.getElementById('away').className = imageSize + " selected";

        // Update Au route
        document.getElementById('au-munich').href = "javascript:mvvRoute('Hopfenhalle,%20Au%20i.d.%20Hallertau', 'M&uuml;nchen%20Hbf');";
        document.getElementById('au-munich').alt = "Au -> Munich";
        document.getElementById('au-munich').title = "Au -> Munich";
        document.getElementById('au-munich-direction').textContent = "Au → Munich";
        document.querySelector('#au-munich .origin-img').src = "/images/au.png";
        document.querySelector('#au-munich .destination-img').src = "/images/munich.png";

        // Update Munich route
        document.getElementById('rohrbach-munich').href = "javascript:mvvRoute('Rohrbach(Ilm)', 'M&uuml;nchen%20Hbf');";
        document.getElementById('rohrbach-munich').alt = "Rohrbach -> Munich";
        document.getElementById('rohrbach-munich').title = "Rohrbach -> Munich";
        document.getElementById('rohrbach-munich-direction').textContent = "Rohrbach → Munich";
        document.querySelector('#rohrbach-munich .origin-img').src = "/images/rohrbach(ilm).png";
        document.querySelector('#rohrbach-munich .destination-img').src = "/images/munich.png";

        // Update Freising route
        document.getElementById('au-freising').href = "javascript:mvvRoute('Hopfenhalle,%20Au%20i.d.%20Hallertau', 'Freising');";
        document.getElementById('au-freising').alt = "Au -> Freising";
        document.getElementById('au-freising').title = "Au -> Freising";
        document.getElementById('au-freising-direction').textContent = "Au → Freising";
        document.querySelector('#au-freising .origin-img').src = "/images/au.png";
        document.querySelector('#au-freising .destination-img').src = "/images/freising.png";

        // Update Freising route
        document.getElementById('freising-munich').href = "javascript:mvvRoute('Freising', 'M&uuml;nchen%20Hbf');";
        document.getElementById('freising-munich').alt = "Freising -> Munich";
        document.getElementById('freising-munich').title = "Freising -> Munich";
        document.getElementById('freising-munich-direction').textContent = "Freising → Munich";
        document.querySelector('#freising-munich .origin-img').src = "/images/freising.png";
        document.querySelector('#freising-munich .destination-img').src = "/images/munich.png";
    }
}

(function ($) {
    var id = 1;
    var greeting = "Welcome Off World (type help)";
    var whoami_msg = "[[g;#FFFF00;]whoami]: your browser info and IP address\r\n";
    var date_msg = "[[g;#FFFF00;]date]: my server date/time\r\n";
    var version_msg = "[[g;#FFFF00;]version]: build of this website\r\n";
    var sw_msg = "[[g;#FFFF00;]aw]: Au weather \r\n";
    var clear_msg = "[[g;#FFFF00;]clear]: clear this terminal screen\r\n";
    var help = whoami_msg + date_msg + sw_msg + version_msg + clear_msg;

    var anim = false;

    term = $('#term1').terminal(function (command, term) {
        var commands = command.split(' ');
        if (commands.length > 0) {
            try {
                switch (commands[0]) {
                    case 'help':
                        term.echo(help);
                        break;

                    case 'date':
                    case 'version':
                    case 'whoami':
                        simpleAjaxCall(command, "query-param");
                        break;

                    case 'aw':
                        au_weather = '//openweathermap.org/city/2954088';
                        window.open(au_weather);
                        break;

                    default:
                        /*jslint es5: true */
                        var result = window.eval(command);
                        if (result !== undefined) {
                            term.echo(result);
                        }
                        break;
                }
            } catch (e) {
                term.echo("[[guib;#FFFF00;]" + e + "] (try `help`)");
            }
        } else {
            term.echo('');
        }
    }, {
        greetings: greeting,
        name: 'term1',
        enabled: false,
        prompt: 'dan@ackerson.de:~ $ ',
        onInit: function (term) {
            //typedPrompt(term, 'help', 250);
        },
        onClear: function (term) {
            term.echo(greeting);
        },
        keydown: function (e) {
            //disable keyboard when animating
            if (anim) {
                return false;
            }
        }
    });

    /* simple ajax call where typed cmd string is SAME as remote URI AND data set */
    function simpleAjaxCall(command, query_param) {
        term.pause();

        //$.jrpc is helper function which creates json-rpc request
        $.jrpc(command, // uri
            id++,
            query_param,
            'post',
            function (data) {
                term.resume();
                if (data.error) {
                    term.error(data.error.message);
                } else {
                    var responseText = jQuery.parseJSON(data.responseText);
                    if (command == 'version') {
                        term.echo("[[g;#FFFF00;]ackerson.de build " + responseText['build'] + "]")
                        window.open(responseText['version']);
                    } else if (command == 'whoami') {
                        term.echo(`${responseText[command]}`);
                    } else term.echo(responseText[command]); // data set
                }
            },
            function (xhr, status, error) {
                term.error('[AJAX] ' + status + ' - Server reponse is: \n' +
                    xhr.responseText);
                term.resume();
            }
        ); // rpc call
    }

    term.mouseout(function () {
        term.focus(false);
    });
    term.mouseover(function () {
        term.focus(true);
    });
})(jQuery);
