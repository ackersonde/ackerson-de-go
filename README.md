[![pipeline status](https://gitlab.com/ackersonde/ackerson-de-go/badges/master/pipeline.svg)](https://gitlab.com/ackersonde/ackerson-de-go/-/commits/master)

# Description
This is my homepage. I primarily use it to watch [baseball games](https://ackerson.de/bb) <img src="https://ackerson.de/images/baseballSmall.png" width="16">

# Installation and Development
0. go get github.com/cosmtrek/air && go install github.com/cosmtrek/air
0. `air` (launch app refresher for dev)
0. http://buildoor.ackerson.de:8080 (now code and fresh builds in the background)

# Building & Running
See [CICD](.gitlab-ci.yml)
