module gitlab.com/ackersonde/ackerson-de-go

go 1.23

require (
	github.com/mssola/user_agent v0.6.0
	github.com/tidwall/gjson v1.18.0
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
)
